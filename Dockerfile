FROM golang:1.16.2-alpine AS build

WORKDIR /go/src/network-debug

COPY go.mod ./
COPY vendor vendor
COPY cmd cmd

RUN go install -v ./cmd/leader-lock


FROM alpine:3.13.2

COPY --from=build /go/bin/leader-lock /usr/local/bin/leader-lock

ENTRYPOINT ["/usr/local/bin/leader-lock"]
