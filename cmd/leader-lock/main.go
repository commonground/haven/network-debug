package main

import (
	"context"
	"flag"
	"os"
	"time"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/leaderelection"
	"k8s.io/client-go/tools/leaderelection/resourcelock"
	"k8s.io/client-go/tools/record"
	"k8s.io/klog/v2"
)

func main() {
	klog.InitFlags(flag.CommandLine)
	flag.Parse()

	config, err := restclient.InClusterConfig()
	if err != nil {
		klog.Fatal(err)
	}

	kubeClient, err := kubernetes.NewForConfig(config)
	if err != nil {
		klog.Fatal(err)
	}

	hostname, err := os.Hostname()
	if err != nil {
		klog.Fatal(err)
	}

	leaderLockName := "leader-lock"
	namespace := os.Getenv("NAMESPACE")

	configMap := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      leaderLockName,
			Namespace: namespace,
		},
	}

	if _, err := kubeClient.CoreV1().ConfigMaps(namespace).Create(context.Background(), configMap, metav1.CreateOptions{}); err != nil && !errors.IsAlreadyExists(err) {
		klog.Fatal(err)
	}

	resLock := &resourcelock.ConfigMapLock{
		ConfigMapMeta: configMap.ObjectMeta,
		Client:        kubeClient.CoreV1(),
		LockConfig: resourcelock.ResourceLockConfig{
			Identity:      hostname,
			EventRecorder: &record.FakeRecorder{},
		},
	}

	leaderelection.RunOrDie(context.Background(), leaderelection.LeaderElectionConfig{
		Lock: resLock,

		WatchDog: leaderelection.NewLeaderHealthzAdaptor(time.Duration(2) * time.Second),

		LeaseDuration: time.Duration(15) * time.Second,
		RenewDeadline: time.Duration(10) * time.Second,
		RetryPeriod:   time.Duration(2) * time.Second,

		Callbacks: leaderelection.LeaderCallbacks{
			OnStartedLeading: func(ctx context.Context) {
				klog.Info("OnStartedLeading")
			},
			OnStoppedLeading: func() {
				klog.Info("OnStoppedLeading")
			},
			OnNewLeader: func(identity string) {
				klog.Info("OnNewLeader: ", identity)
			},
		},
	})
}
